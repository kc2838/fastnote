package com.example.yang.fastnote;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.DatePicker;

public class getDate extends AppCompatActivity {
    DatePicker getDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_date);

        getDate = (DatePicker) findViewById(R.id.datePicker);
    }
    @Override
    public void onBackPressed(){
        Intent intent=new Intent();
        intent.putExtra("GETDATE", String.valueOf(getDate.getMonth())+" - "+String.valueOf(getDate.getDayOfMonth())+" - "+String.valueOf(getDate.getYear()));
        setResult(10, intent);
        finish();
    }
}
