package com.example.yang.fastnote;

import java.util.Vector;

/**
 * Created by yang on 11/15/15.
 */
public class NotesCollection {
    private Vector<Note> list = new Vector<Note>();

    public void addNote(String information, String Date){
        list.add(new Note(information,Date));
    }
    public void remove(int i){
        list.remove(i);
    }
    public Vector<Note> getList(){
        return list;
    }

    public Note get(int i){
        return list.get(i);
    }


}
