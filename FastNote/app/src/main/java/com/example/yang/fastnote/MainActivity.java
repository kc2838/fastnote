package com.example.yang.fastnote;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import junit.framework.Test;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ArrayList<String> listItems;
    private ArrayAdapter<String> adapter;
    private Button newNote;
    private Button Remover;
    private ListView noteList;
    private NotesCollection notes;
    private int counter = 0;
    private int pos = 0, previous = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Setting Views
        listItems = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listItems);
        newNote = (Button) findViewById(R.id.add);
        Remover = (Button) findViewById(R.id.remove);
        noteList = (ListView) findViewById(R.id.listView);
        Intent intent = getIntent();
        notes = new NotesCollection();
        Remover.setEnabled(false);

        //Read in from a file database
        FileInputStream fis;
        try {
            fis = openFileInput("notesdatabase");
            byte[] input = new byte[fis.available()];
            while (fis.read(input) != -1) {
                String info = "";
                String dat = "";
                String data = new String(input);
                String[] received = data.split("&");
                System.out.println("DATAAAAA:------>  " + data);
                for (int i = 0; i < received.length; i += 2) {
                    if (received[i] != " " && received[i + 1] != " ") {
                        info = received[i];
                        dat = received[i + 1];
                        notes.addNote(info, dat);
                        listItems.add(notes.get(counter).toString());
                        counter++;
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //On item selected
        noteList.setAdapter(adapter);
        noteList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(notes.getList().size()<=5){
                previous = pos;
                if (parent.getChildAt(previous) != null)
                    parent.getChildAt(previous).setBackgroundColor(Color.TRANSPARENT);
                pos = position;
                //Toast.makeText(getApplicationContext(),String.valueOf(position),Toast.LENGTH_SHORT).show();
                noteList.getChildAt(pos).setBackgroundColor(Color.GRAY);
                Remover.setEnabled(true);
                }else{
                    pos = position;
                    Remover.setEnabled(true);
                }

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 99){
            try {
                ArrayList<String> thingsYouSaid = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                int here = -1;
                for (int i = 0; i < thingsYouSaid.size(); i++) {
                    if (thingsYouSaid.get(i).contains("Date") || thingsYouSaid.get(i).contains("date")) {
                        here = i;
                        break;
                    }
                }
                if (here != -1) {
                    String result = thingsYouSaid.get(here).toString();
                    Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
                    //if(result.contains("date"))
                    String[] getter = result.split("date");
                    System.out.println("CARALEOOOO1--------->" + getter[0]);
                    System.out.println("CARALEEOOOO2--------->" + getter[1]);
                    try {
                        String message1 = getter[0];
                        String message2 = getter[1];
                        FileOutputStream fos = openFileOutput("notesdatabase", Context.MODE_PRIVATE);
                        for (int i = 0; i < notes.getList().size(); i++) {
                            String helper = notes.get(i).getNote() + "&";
                            fos.write(helper.getBytes());
                            helper = notes.get(i).getDate() + "&";
                            fos.write(helper.getBytes());
                        }
                        notes.addNote(message1, message2);
                        listItems.add(notes.get(counter).toString());
                        counter++;
                        noteList.setAdapter(adapter);
                        message1 += "&";
                        message2 += "&";
                        fos.write(message1.getBytes());
                        fos.write(message2.getBytes());
                        fos.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "'Date' word key not found or incomplete sentence", Toast.LENGTH_LONG);
                }
            }
            catch (Exception e){
                System.out.println("ERROOOOOOOOOOOOU");
            }
        }
        if (requestCode == 3) {
            String test = data.getStringExtra("TEST");
            if(test.equals("good")) {
                String message1 = data.getStringExtra("INFORMATION");
                String message2 = data.getStringExtra("DATE");
                if (message1 != null && message2 != null) {
                    try {
                        notes.get(pos).setDate(message2);
                        notes.get(pos).setNote(message1);
                        listItems.set(pos,notes.get(pos).toString());
                        try {
                            FileOutputStream fos = openFileOutput("notesdatabase", Context.MODE_PRIVATE);
                            for (int i = 0; i < notes.getList().size(); i++) {
                                String helper = notes.get(i).getNote() + "&";
                                fos.write(helper.getBytes());
                                helper = notes.get(i).getDate() + "&";
                                fos.write(helper.getBytes());
                            }
                            noteList.setAdapter(adapter);
                            fos.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 2) {
            String test = data.getStringExtra("TEST");
            if(test.equals("good")) {
                String message1 = data.getStringExtra("INFORMATION");
                String message2 = data.getStringExtra("DATE");
                if (message1 != null && message2 != null) {
                    try {
                        try {
                            FileOutputStream fos = openFileOutput("notesdatabase", Context.MODE_PRIVATE);
                            for (int i = 0; i < notes.getList().size(); i++) {
                                String helper = notes.get(i).getNote() + "&";
                                fos.write(helper.getBytes());
                                helper = notes.get(i).getDate() + "&";
                                fos.write(helper.getBytes());
                            }
                            notes.addNote(message1, message2);
                            listItems.add(notes.get(counter).toString());
                            counter++;
                            noteList.setAdapter(adapter);
                            message1 += "&";
                            message2 += "&";
                            fos.write(message1.getBytes());
                            fos.write(message2.getBytes());
                            fos.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }

    public void AddNewNote(View view) {
        Intent intent = new Intent(this, getInputs.class);
        intent.putExtra("INFO", "");
        intent.putExtra("DATE", "");
        startActivityForResult(intent, 2);
        noteList.setAdapter(adapter);
    }

    public void Remove(View view) {
        if (notes.getList().size() != 0) {
            counter--;
            System.out.println(pos);
            notes.remove(pos);
            listItems.remove(pos);
            noteList.setAdapter(adapter);
            try {
                FileOutputStream fos = openFileOutput("notesdatabase", Context.MODE_PRIVATE);
                for (int i = 0; i < notes.getList().size(); i++) {
                    String helper = notes.get(i).getNote() + "&";
                    fos.write(helper.getBytes());
                    helper = notes.get(i).getDate() + "&";
                    fos.write(helper.getBytes());
                }
            } catch (Exception e) {
                e.printStackTrace();

            }

        }
    }
    public void Update(View view){
        Intent intent = new Intent(this, getInputs.class);
        intent.putExtra("INFO",notes.get(pos).getNote());
        intent.putExtra("DATE",notes.get(pos).getDate());
        startActivityForResult(intent, 3);
        noteList.setAdapter(adapter);
    }
    public void getSpeech(View view){
        Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
        try {
            startActivityForResult(i, 99);
        } catch (Exception e) {
            Toast.makeText(this, "Error initializing speech to text engine.", Toast.LENGTH_LONG).show();
        }
    }
    public void getInfo(View view){
        Intent intent = new Intent(this, showInfo.class);
        startActivity(intent);
    }
}


