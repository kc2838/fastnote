package com.example.yang.fastnote;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.util.ArrayList;

public class getInputs extends AppCompatActivity {
    private EditText InformationText;
    private TextView Date;
    String message1="",message2="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_inputs);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        InformationText = (EditText) findViewById(R.id.editText);
        Date = (TextView) findViewById(R.id.editText2);

        Intent intent = getIntent();
        String message1 = intent.getStringExtra("INFO");
        String message2 = intent.getStringExtra("DATE");
        if(!message1.equals("") && !message2.equals("")) {
            InformationText.setText(message1);
            Date.setText(message2);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    @Override
    public void onBackPressed(){
        Intent intent = new Intent();
        intent.putExtra("TEST", "bad");
        setResult(2, intent);
        finish();
    }
    public void back(View view){
            Intent intent=new Intent();
            if(!InformationText.getText().toString().equals("") && !Date.getText().toString().equals("")) {
                intent.putExtra("INFORMATION", InformationText.getText().toString());
                intent.putExtra("DATE", Date.getText().toString());
                if (!message1.equals("") && !message2.equals("")) {
                    intent.putExtra("TEST", "bad");
                    setResult(2, intent);
                } else{
                    intent.putExtra("TEST", "good");
                    setResult(3, intent);
                }
                finish();
            }
            else{
                intent.putExtra("TEST", "bad");
                setResult(2, intent);
                finish();
            }



    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 10) {
            String message = data.getStringExtra("GETDATE");
            Date.setText(message);
        }

    }
    public void getDate(View view){
        Intent intent = new Intent(this, getDate.class);
        startActivityForResult(intent,10);
    }
}
