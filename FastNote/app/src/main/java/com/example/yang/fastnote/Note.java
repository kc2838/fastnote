package com.example.yang.fastnote;

/**
 * Created by yang on 11/15/15.
 */
public class Note {
    private String information;
    private String Date;
    private int ID;

    public Note(String information, String Date){
        this.information = information;
        this.Date = Date;
    }
    public String toString(){
        String s;
        s="Information: "+information;
        s+="\nDate "+Date;
        return s;
    }
    public void setNote(String information){
        this.information = information;
    }
    public void setDate(String Date){
        this.Date = Date;
    }
    public String getNote(){
        return  information;
    }
    public String getDate(){
        return Date;
    }
}
