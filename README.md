# README #

This application is called “Fast Note” because it is supposed to save notes in an easy and fast way. The user just needs to hit a button at the application and then it opens a microphone prepared to listen to a speech and convert it into a new note  that is going to be saved in a file Database. The user can also create a new note manually by typing text, and manage then with such operations like “Add”, “Remove” and “Update”.
*  https://bitbucket.org/kc2838/fastnote

Main Activity: This is the main class and it manages the first GUI to the user. When it starts, retrieve any notes stored at the file Database and then puts in a list view. It has some button to manage the notes at the application and also a “Info” button to guide the user.
GetDate: This class display a date picker and the user can select a due date to it's note there. When finished, returns the selected date.
GetInputs: This class is used for displaying input fields to the user so it can save a new note and when it finishes, returns the notes information to the main class.
Note: This is a class that is supposed to store notes informations such it's text and also its due date, also with some methods as “toString()” that returns a string with all its variables information.

NotesCollection: This class is used to stored many notes and make some operations with it's list.
ShowInfo: This class displays a textView for the user with some information about the application.

UML:

![1.png](https://bitbucket.org/repo/nd4obk/images/2096033326-1.png)